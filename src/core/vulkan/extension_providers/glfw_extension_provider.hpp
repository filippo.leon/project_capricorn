/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leon@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include "core/vulkan/exceptions.hpp"

#include "core/vulkan/extension_providers/iextension_provider.hpp"

inline constexpr bool DYNAMIC_GLFW_LINK = false;

namespace vulkan {

class GlfwExtensionProvider : public IExtensionProvider {
public:
    GlfwExtensionProvider() {
        if (DYNAMIC_GLFW_LINK && !glfwVulkanSupported()) {
            BOOST_THROW_EXCEPTION(VulkanException{} << VkMessage_Info("GLFW_NO_VULKAN"));
        }
    }

    virtual std::vector<const char*> Extensions() override {
        std::vector<const char*> ret;
        uint32_t count;
        const char** extensions = glfwGetRequiredInstanceExtensions(&count);
        for (uint32_t i = 0u; i < count; ++i) {
            ret.push_back(extensions[i]);
        }
        return ret;
    }
};

}
