/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leon@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#define GLFW_INCLUDE_VULKAN
#include "core/vulkan/instance.hpp"

#include <algorithm>

#include "core/vulkan/exceptions.hpp"
#include <iostream>
namespace vulkan {

Instance::Instance(std::vector<IExtensionProvider*> providers) {
    std::vector<const char *> exts;
    for (IExtensionProvider *provider: providers) {
        std::vector<const char *> otherExts = provider->Extensions();
        exts.insert(exts.begin(), otherExts.begin(), otherExts.end());
    }
    VkApplicationInfo appInfo;

    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = "Hello Triangle";
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.apiVersion = VK_API_VERSION_1_1;

    VkInstanceCreateInfo info;

    info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    info.pApplicationInfo = &appInfo;
    info.enabledExtensionCount = (int)exts.size();
    info.ppEnabledExtensionNames = exts.data();
    info.enabledLayerCount = 0;

    std::cout << "All good" << std::endl;
    VkResult result = vkCreateInstance(&info, nullptr, &m_instance);
    std::cout << "All good" << std::endl;
    if (result != VK_SUCCESS) {
        std::cout << "lol" << std::endl;
        BOOST_THROW_EXCEPTION(VulkanException{} << VkResultErrno_Info(result));
    }
}

} // namepsace vulkan