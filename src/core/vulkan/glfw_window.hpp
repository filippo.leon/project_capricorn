/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leon@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

//#include <vulkan/vulkan_core.h>

#include "core/vulkan/instance.hpp"
#include "core/math/vec.hpp"
#include "core/vulkan/exceptions.hpp"
#include "core/debug/assert.hpp"

namespace vulkan {

class GlfwWindow {
public:
    GlfwWindow(vulkan::Instance &instance, math::vec<int, 2> size, std::string title = "") {
        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        GLFWwindow *window = glfwCreateWindow(size.x, size.y, title.c_str(), nullptr, nullptr);
        ASSERT(window != nullptr, "Failed to create GLFW window!");

        VkSurfaceKHR surface;
        VkResult result = glfwCreateWindowSurface(instance.get(), window, nullptr, &surface);
        if (result != VK_SUCCESS) {
             BOOST_THROW_EXCEPTION(VulkanException{}
                 << VkResultErrno_Info(result)
                 << VkMessage_Info("Couldn't create windows surface."));
        }
    }
};

} // namepsace vulkan