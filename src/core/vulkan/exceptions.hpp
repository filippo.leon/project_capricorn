/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leon@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <boost/throw_exception.hpp>
#include <boost/exception/exception.hpp>
#include <boost/exception/info.hpp>
#include <boost/exception/diagnostic_information.hpp>
#include <boost/exception/errinfo_at_line.hpp>
#include <boost/exception/errinfo_file_name.hpp>
#include <boost/exception/errinfo_api_function.hpp>

//#include <vulkan/vulkan_core.h>

#include <fmt/format.h>

namespace vulkan {

struct VkResultErrno_Tag;
using VkResultErrno_Info = boost::error_info<VkResultErrno_Tag, VkResult>;

struct VkMessage_Tag;
using VkMessage_Info = boost::error_info<VkMessage_Tag, std::string>;

class VulkanException : public virtual boost::exception, public virtual std::exception {
    virtual const char* what() const noexcept {
        return boost::diagnostic_information_what(*this);
    }
};

} // namepsace vulkan