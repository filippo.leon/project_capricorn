/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <vector>

//#include <vulkan/vulkan_core.h>

#include "core/vulkan/extension_providers/iextension_provider.hpp"

namespace vulkan {

class Instance {
public:
    Instance(std::vector<IExtensionProvider*> providers);

    VkInstance &get() {
        return m_instance;
    }
private:
    VkInstance m_instance;
};

}
