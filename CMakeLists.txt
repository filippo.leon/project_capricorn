cmake_minimum_required(VERSION 3.12)
project(project_capricorn)

set(CMAKE_CXX_STANDARD 17)

if (GCC)
    add_definitions("-pedantic -Wall -Wextra -ffast-math")
endif(GCC)

set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake-modules)

add_subdirectory(external/fmt EXCLUDE_FROM_ALL)

set(ENABLE_TESTS ON)

message(STATUS "Compiler ${CMAKE_CXX_COMPILER_ID} ${CMAKE_CXX_COMPILER_VERSION}")

find_package(Threads)

set(VULKAN_STATIC_LIBRARY 0)
set(GLFW_ROOT "${CMAKE_CURRENT_SOURCE_DIR}/glfw-3.2.1")

set(GLFW_BUILD_DOCS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_TESTS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_EXAMPLES OFF CACHE BOOL "" FORCE)

find_package(Vulkan REQUIRED)
if (${Vulkan_FOUND})
    message(STATUS "Vulkan found at: ${Vulkan_LIBRARIES}")
    set(VULKAN_LIBRARY ${VULKAN_LIBRARIES})
endif()

#find_package(glfw3)
#if(NOT GLFW3_FOUND)
    message(STATUS "Glfw3 NOT found, using in-source files!")
    add_subdirectory(${GLFW_ROOT})
#else()
    message(STATUS "Glfw found at: ${GLFW3_LIBRARIES}")
#endif()

#set(Boost_USE_STATIC_LIBS       ON)
#set(Boost_USE_STATIC_RUNTIME    ON)
#set(Boost_USE_MULTITHREADED     ON)
find_package(Boost REQUIRED regex system filesystem)


set(RESOURCES_PATH "resources")
file(COPY ${CMAKE_SOURCE_DIR}/${RESOURCES_PATH} DESTINATION ${CMAKE_BINARY_DIR}/${RESOURCES_PATH})

add_subdirectory(src)
if (ENABLE_TESTS)
    add_subdirectory(tests)
endif()