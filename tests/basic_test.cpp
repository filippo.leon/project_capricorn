/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <core/vulkan/instance.hpp>
#include <core/vulkan/exceptions.hpp>
#include <core/vulkan/extension_providers/glfw_extension_provider.hpp>
#include <core/vulkan/glfw_window.hpp>

#include <iostream>
int main() {

    glfwSetErrorCallback([] (int err, const char *s) {
        std::cout << "Error" << s << std::endl;
    });
    if(!glfwInit()) {
        std::cout << "Error" << std::endl;
    }
std::cout << DYNAMIC_GLFW_LINK << " " << glfwVulkanSupported() << std::endl;


    std::vector<vulkan::IExtensionProvider*> exc;

    auto glfwProvider = std::make_unique<vulkan::GlfwExtensionProvider>();
    exc.push_back(glfwProvider.get());

    vulkan::Instance inst(exc);

    //vulkan::GlfwWindow win(inst, math::vec<int, 2>{500, 500}, "My Window");
}